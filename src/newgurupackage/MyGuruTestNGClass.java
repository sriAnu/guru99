package newgurupackage;
import static Utils.Utils.BASE_URL;
import static Utils.Utils.CHROME_DRIVER_LOCATION;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;


import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;
import Utils.Utils;
import newgurupackage.MyGuruBaseClass;

import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;

import API.UiApi;

import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import Utils.ReadFromExcel;

public class MyGuruTestNGClass {
	
	public WebDriver chromedriver;
	ReadFromExcel readfromexcel;
	MyGuruBaseClass mygurubaseclass;
	UiApi uiapi;
	
	@BeforeMethod
	public void BeforeMethod(){
		mygurubaseclass = new MyGuruBaseClass();
		uiapi = new UiApi();
		chromedriver = mygurubaseclass.MyGuruBaseClass();
			
	}

	@Test(description ="Login Tests", dataProvider="ManagerIDdetails")
	public void verifyLogin(String managerId){
	
	for(int i=0;i<3;i++)
	{
		try{
		WebElement uname = chromedriver.findElement(By.name("uid"));
		if(uname.isDisplayed())
		{	System.out.println(i);
			uname.sendKeys(readfromexcel.readfromExcelSheet("U:\\Selenium\\newguruproject\\src\\testdata", "testdatafile.xlsx","data",i, 0));
			WebElement pwd = chromedriver.findElement(By.name("password"));
			if(pwd.isDisplayed())
			{
				pwd.sendKeys(readfromexcel.readfromExcelSheet("U:\\Selenium\\newguruproject\\src\\testdata", "testdatafile.xlsx","data",i, 1));
				WebElement loginbtn = chromedriver.findElement(By.name("btnLogin"));
				loginbtn.click();
				try{
					if(uiapi.isAlertPresent(chromedriver)){
						String alertText = chromedriver.switchTo().alert().getText();
						String expectedAlertText = "User or Password is not valid";
						assertEquals(alertText,expectedAlertText);
						chromedriver.switchTo().alert().accept();
						
					}
					else{
						String title = chromedriver.getTitle();
						String expectedTitle = "Guru99 Bank Manager HomePage";
						assertEquals(title,expectedTitle);
						TakesScreenshot scrShot =((TakesScreenshot)chromedriver);
						File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
						File DestFile= new File("U:\\Selenium\\newguruproject\\src\\screenshots\\1.jpg");

		                //Copy file at destination

		                FileUtils.copyFile(SrcFile, DestFile);
						List<WebElement> welcomeText = chromedriver.findElements(By.className("heading3"));
						for(WebElement e : welcomeText)
						{
							if(e.getText().contains("Manger Id"))
							{
								String welcomeTextActual = e.getText();
								assertTrue(welcomeTextActual.contains(managerId));
								
															
							}
							
						}
						
									
					}
					
				}
				catch(Exception e)
				{
					System.out.println("Exception"+e);
					continue;
				}
				
			}
	}
		}
		catch(Exception e){
			
		}
	}
	
	
	
	}
	
	@DataProvider(name="ManagerIDdetails")
	public Object[][] getDataFromDataprovider(){
	      	        return new Object[][] {
	                { "mngr155720"}                
	      	        };
	            }
	
		
	
	
		// isAlertPresent()
	
	@Test(description ="add customer")
	public void TestAddCustomer(){
		 mygurubaseclass.Login(chromedriver);
		 chromedriver.findElement(By.linkText("New Customer")).click();
		 chromedriver.findElement(By.name("name")).sendKeys("Sriie");
		 chromedriver.findElement(By.xpath("//input[@value='f']")).click();
		 chromedriver.findElement(By.id("dob")).sendKeys("11/04/1995");
		 chromedriver.findElement(By.name("addr")).sendKeys("Los Angeles");
		 chromedriver.findElement(By.name("city")).sendKeys("Waltham");
		 chromedriver.findElement(By.name("state")).sendKeys("Boston");
		 chromedriver.findElement(By.name("pinno")).sendKeys("024532");
		 chromedriver.findElement(By.name("telephoneno")).sendKeys("6754328910");
		 chromedriver.findElement(By.name("emailid")).sendKeys("sri8@gmail.com");
		 chromedriver.findElement(By.name("password")).sendKeys("Waltham");
		 chromedriver.findElement(By.xpath("//input[@value ='Submit']")).click();	
		 String ExpectedSuccessMessage = "Customer Registered Successfully!!!";
		 String ActualMessage = chromedriver.findElement(By.xpath("p[@class = 'heading3']")).getText();
		 assertEquals(ActualMessage,ExpectedSuccessMessage);
		 System.out.println("Customer added successfully");
		 		
	}
	
	@org.testng.annotations.AfterMethod
	public void AfterMethod(){
		chromedriver.findElement(By.linkText("Log out")).click();
		chromedriver.quit();
	}

}
