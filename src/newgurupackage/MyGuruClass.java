package newgurupackage;
import org.openqa.selenium.*;
//import org.openqa.selenium.WebDriver.Options;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import static Utils.Utils.BASE_URL;
import static Utils.Utils.CHROME_DRIVER_LOCATION;
//import static Utils.Utils.PASSWORD;
//import static Utils.Utils.USERNAME;

import java.util.concurrent.TimeUnit;

import Utils.ReadFromExcel;
//import Utils.ReadFromExcel.*;
public class MyGuruClass
{
	public static void main(String[] args ){
		
		WebDriver chromedriver = SetupChromeBrowser();
		ReadFromExcel readfromexcel = new ReadFromExcel();
		chromedriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//base URL of the application
		chromedriver.get(BASE_URL);
		
	for(int i=0;i<3;i++)
	{
		try{
		WebElement uname = chromedriver.findElement(By.name("uid"));
		if(uname.isDisplayed())
		{	System.out.println(i);
			uname.sendKeys(readfromexcel.readfromExcelSheet("U:\\Selenium\\newguruproject\\src\\testdata", "testdatafile.xlsx","data",i, 0));
			WebElement pwd = chromedriver.findElement(By.name("password"));
			if(pwd.isDisplayed())
			{
				pwd.sendKeys(readfromexcel.readfromExcelSheet("U:\\Selenium\\newguruproject\\src\\testdata", "testdatafile.xlsx","data",i, 1));
				WebElement loginbtn = chromedriver.findElement(By.name("btnLogin"));
				loginbtn.click();
				try{
					if(isAlertPresent(chromedriver)){
						String alertText = chromedriver.switchTo().alert().getText();
						 
						if(alertText.equals("User or Password is not valid"))
						{
							System.out.println("Pass: Invalid login popup displayed");
							chromedriver.switchTo().alert().accept();
						}
						else
						{
							System.out.println("Fail: wrong Invalid popup displayed");
							chromedriver.switchTo().alert().accept();
						}
					}
					else{
						String title = chromedriver.getTitle();
						if(title.equals("Guru99 Bank Manager HomePage"))
						{
							System.out.println("Pass:User can successfully login to the application");
						}
						else
						{
							System.out.println("Fail:User did not login correctly");
						}
									
						
					}
					
				}
				catch(Exception e)
				{
					System.out.println("Exception");
				}
				
			}
		}
		}
		catch(Exception e){
			
		}
	}
		
		
		
		
		
		
		
		/*	try{
		WebElement uname = chromedriver.findElement(By.name("uid"));
		if(uname.isDisplayed())
		{
			uname.sendKeys(USERNAME);
			WebElement pwd = chromedriver.findElement(By.name("password"));
			if(pwd.isDisplayed())
			{
				pwd.sendKeys(PASSWORD);
				WebElement loginbtn = chromedriver.findElement(By.name("btnLogin"));
				loginbtn.click();
				String title = chromedriver.getTitle();
				if(title.equals("Guru99 Bank Manager HomePage"))
				{
					System.out.println("Pass:User can successfully login to the application");
				}
				else
				{
					System.out.println("Fail:User did not login correctly");
				}
			}
		}
		
		else
		{
			System.out.println("username textbox missing");
		}
		}
		catch(Exception e)
		{
		System.out.println("Could be missing object");
	}*/
		
		chromedriver.quit();
		
		
	}
	
	public static WebDriver SetupChromeBrowser()
	{
		System.setProperty("webdriver.chrome.driver",CHROME_DRIVER_LOCATION );

		ChromeOptions options = new ChromeOptions();
		options.addArguments("user-data-dir = C:\\Users\\Srilatha\\AppData\\Local\\Google\\Chrome\\User Data");
		options.addArguments("--start-maximized");
		
		return new ChromeDriver(options);
		
	}
	
	public static boolean isAlertPresent(WebDriver driver) 
	{ 
	    try 
	    { 
	    	driver.switchTo().alert(); 
	        return true; 
	    }   // try 
	    catch (NoAlertPresentException Ex) 
	    { 
	        return false; 
	    }   // catch 
	}   // isAlertPresent()
	
}


