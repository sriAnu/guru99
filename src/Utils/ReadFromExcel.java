package Utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadFromExcel {
 public String readfromExcelSheet(String filepath, String ExcelName,String Sheetname,int row, int column) throws IOException {
	 File src = new File(filepath+"\\"+ExcelName);
	 System.out.println(src.getPath());
	 
	 FileInputStream inputStream = new FileInputStream(src);
	 
	  //testdataworkbook = null;
	 XSSFWorkbook testdataworkbook=null;
	 
	 String fileextension = ExcelName.substring(ExcelName.indexOf("."));
	 if(fileextension.equals(".xlsx")){
		 
		 testdataworkbook = new XSSFWorkbook(inputStream);
		 System.out.println("yoo");
		  
	 }
	 
	 XSSFSheet testdatasheet = testdataworkbook.getSheetAt(0);
	 return (testdatasheet.getRow(row).getCell(column).toString());
	 
 }
}
